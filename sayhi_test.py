import unittest
from sayhi import sayHi

class sayHiTest(unittest.TestCase):
    # remember to put test always as pre-fix
    def test_simple(self):
        self.assertEqual(sayHi('Luis'), 'Hi Luis!')
        self.assertTrue(isinstance(sayHi('Luis'), str))
